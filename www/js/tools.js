function loadAsBlob( url, callback ) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'blob';
  xhr.onload = function(e) {
    if (this.status == 200) {
      var blob = this.response;
      callback(blob);
    }
  };
  xhr.send();
}


const ExcelToJSON = function() {

  this.parseExcel = function(file, callback) {
    const reader = new FileReader();

    reader.onload = function(e) {
      // console.log('e',e, this);
      // var data = e.target.result();
      const data = this.result;

      const workbook = XLSX.read(data, { type: 'binary' });
      // var sheets = [];
      workbook.SheetNames.forEach(function(sheetName) {
        // Here is your object
        const XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        // var json_object = JSON.stringify(XL_row_object);
        callback(XL_row_object, sheetName);
      })
    };
    reader.onerror = function(ex) {
      console.log(ex);
    };

    reader.readAsBinaryString(file);
  };

};
