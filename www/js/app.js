// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.directives','app.services',])

.config(function($ionicConfigProvider, $sceDelegateProvider){

  $sceDelegateProvider.resourceUrlWhitelist([ 'self','*://www.youtube.com/**', '*://player.vimeo.com/video/**']);

})

.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    const excel = new ExcelToJSON();
    $rootScope.privs = null;
    $rootScope.params = null;
    $rootScope.vars = null;
    loadAsBlob('xls/privileges.xlsx', function(blob){
    // loadAsBlob('../../../Google Диск/Projects/Priveleges/privileges.xlsx', function(blob){
      excel.parseExcel(blob, function(sheet, sheetName) {
        console.log('sheetName',sheetName);
        console.log('sheet', sheet);

        if (sheetName==='Льготы') {
          $rootScope.privs = sheet;
          // var params = _.pluck(sheet,'Параметры');
          // var paramsArr = [];
          // _.each(params, function(p){
          //   paramsArr = paramsArr.concat(p.split(','));
          // });
          // paramsTr = _.map(paramsArr, function(p){
          //   // console.log('p',p.trim());
          //   return p.trim();
          // });
          // console.log('paramsTr',paramsTr);
        } else if (sheetName==='Параметры') {
          const keys = _.pluck(sheet,'Имя');
          $rootScope.params = _.object(keys,sheet);
          // console.log('$rootScope.params', $rootScope.params);

        } else if (sheetName==='Переменные') {
          $rootScope.vars = sheet;
        } else {
          console.warn('Wrong sheet name', sheetName);
        }
      });
    })

  });
})

/*
  This directive is used to disable the "drag to open" functionality of the Side-Menu
  when you are dragging a Slider component.
*/
.directive('disableSideMenuDrag', ['$ionicSideMenuDelegate', '$rootScope', function($ionicSideMenuDelegate, $rootScope) {
    return {
        restrict: "A",
        controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {

            function stopDrag(){
              $ionicSideMenuDelegate.canDragContent(false);
            }

            function allowDrag(){
              $ionicSideMenuDelegate.canDragContent(true);
            }

            $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
            $element.on('touchstart', stopDrag);
            $element.on('touchend', allowDrag);
            $element.on('mousedown', stopDrag);
            $element.on('mouseup', allowDrag);

        }]
    };
}])

/*
  This directive is used to open regular and dynamic href links inside of inappbrowser.
*/
.directive('hrefInappbrowser', function() {
  return {
    restrict: 'A',
    replace: false,
    transclude: false,
    link: function(scope, element, attrs) {
      var href = attrs['hrefInappbrowser'];

      attrs.$observe('hrefInappbrowser', function(val){
        href = val;
      });

      element.bind('click', function (event) {

        window.open(href, '_system', 'location=yes');

        event.preventDefault();
        event.stopPropagation();

      });
    }
  };
});
