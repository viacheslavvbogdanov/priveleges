angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider


      .state('intro', {
    url: '/intro',
    templateUrl: 'templates/intro.html',
    controller: 'introCtrl'
  })

  .state('start', {
    url: '/start',
    templateUrl: 'templates/start.html',
    controller: 'startCtrl'
  })

  .state('poll', {
    cache: false,
    url: '/poll',
    templateUrl: 'templates/poll.html',
    controller: 'pollCtrl'
  })

  .state('report', {
    cache: false,
    url: '/report',
    templateUrl: 'templates/report.html',
    controller: 'reportCtrl'
  })

  .state('privilege', {
    cache: false,
    url: '/privilege',
    templateUrl: 'templates/privilege.html',
    controller: 'privilegeCtrl'
  })

  .state('about', {
    url: '/about',
    templateUrl: 'templates/about.html',
    controller: 'aboutCtrl'
  })

  .state('buy', {
    url: '/buy',
    templateUrl: 'templates/buy.html',
    controller: 'buyCtrl'
  })

  .state('print', {
    cache: false,
    url: '/print',
    templateUrl: 'templates/print.html',
    controller: 'printCtrl'
  })

$urlRouterProvider.otherwise('/start')


});
