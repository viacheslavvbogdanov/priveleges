angular.module('app.controllers', [])

.controller('introCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('startCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('pollCtrl', ['$scope', '$stateParams', '$rootScope', '$timeout', '$ionicScrollDelegate',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $rootScope, $timeout, $ionicScrollDelegate) {
  $scope.questions=[];
  $scope.showPrivsBtn = false;

  var context = {};
  var currentPriv = 0; //TODO back to 0

  function evalInContext(js, context) {
    'use strict'; //"
    //# Return the results of the in-line anonymous function we .call with the passed context
    return function() {
      'use strict';
      console.log('this',this, typeof zahoronenie);
      return eval(js);
    }.call(context);
  }

  function next() {
    const priv = $rootScope.privs[currentPriv];
    if (priv) {
      evalPrivilegeFormula($rootScope.privs[currentPriv]);
    } else {
      $scope.showPrivsBtn = true;
    }
    $ionicScrollDelegate.scrollBottom();
  }

  $scope.choose =function(button, question, answer) {
    console.log('question, answer', question, answer);
    _.each(question.buttons, function(button) {
      button.selected = false;
    });
    button.selected = true;
    question['Ответ']=answer;
    window[question['Имя']]=answer;
    next();
  };

  $scope.setFloat =function(button, question) {
    console.log('question, answer', question);
    question.answered = true;
    question['Ответ']=question.value;
    console.log('question',question);
    window[question['Имя']]=question.value;
    next();
  };

  function addQuestion(param) {
    console.log('addQuestion', param);
    const q = $rootScope.params[param];
    if (q['Тип']=='bool') {
      q.buttons=[
        {title:'Нет', value:false},
        {title:'Да',  value:true}
      ]
    } else if (q['Тип']=='list') {
      const values = q['Значения'].split(',');
      q.buttons = [];
      _.each(values, function(value) {
        q.buttons.push({title:value, value:value})
      })
    }
    console.log('q', q);
    $scope.questions.push(q);
    console.log('$scope.questions', $scope.questions);


  }

  function evalPrivilegeFormula(priv) {
    try {
      const formula = priv['Формула'];
      console.log('formula', formula, context);
      var subs = evalInContext(priv['Формула'], context);
      if (typeof subs==='number') subs = Math.round(subs*100)/100;
      priv['Сумма'] = subs;
      console.log('priv[\'Сумма\']', priv['Сумма']);
      currentPriv++;
      next();

    } catch(e) {
      console.warn('e',e);
      // console.log('e.name',e.name);
      if (e.name==='ReferenceError') {
        const undefinedParam = e.message.split(' ')[0];
        console.log('undefinedParam', undefinedParam);
        addQuestion(undefinedParam);
      } else console.error(e);
    }

  }


  $timeout(function(){
    next();
  },1000);

}])

.controller('reportCtrl', ['$scope', '$stateParams', '$rootScope',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $rootScope) {
  console.log('$rootScope.privs',$rootScope.privs);

}])

.controller('privilegeCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('aboutCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('buyCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])

.controller('printCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
